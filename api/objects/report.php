<?php

class Report
{
    private $conn;
    private $table_name = "reports";

    public $id;
    public $bank_account_id;
    public $amount;
    public $date_created;
    
    public function __construct($db)
    {
        $this->conn = $db;
    }

    function insertReport($data) {
        $data = (Object)$data;      
        
        // query to insert record
        $query = "INSERT INTO {$this->table_name} SET increase_amount=:increase_amount, increase_amount_usd=:increase_amount_usd, count_deposits=:count_deposits, date_created=:date_created";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->increase_amount = htmlspecialchars($data->increase_amount);
        $this->increase_amount_usd = htmlspecialchars($data->increase_amount_usd);
        $this->count_deposits = htmlspecialchars($data->count_deposits);
        $this->date_created = htmlspecialchars($data->date_created);
        
        // bind values
        $stmt->bindParam(":increase_amount", $this->increase_amount);
        $stmt->bindParam(":increase_amount_usd", $this->increase_amount_usd);
        $stmt->bindParam(":count_deposits", $this->count_deposits);
        $stmt->bindParam(":date_created", $this->date_created);
        
        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;
    }
    
}