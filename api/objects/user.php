<?php

class User
{
    private $conn;
    private $table_name = "users";

    public $id;
    public $user_id;
    public $currency;
    public $iban;
    public $date_created;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    /*
     * Get users
     */
    function getAll()
    {
        $query = "SELECT u.id, u.name
        FROM {$this->table_name} as u
        ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function getBankAccountsForUser($userId) {
        $userId = htmlspecialchars(strip_tags($userId));

        $query = "SELECT ba.id, ba.currency, ba.iban
                  FROM bank_accounts as ba
                  WHERE ba.user_id = {$userId}
                  ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $res = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $row['deposits'] = $this->getDepositsForBankAccount($row['id']);
            $res[] = $row;
        }

        return $res;
    }

    function getDepositsForBankAccount($bankAccountId) {
        $bankAccountId = htmlspecialchars(strip_tags($bankAccountId));

        $query = "SELECT dp.id, dp.amount, dp.date_created
                  FROM deposits as dp
                  WHERE dp.bank_account_id = {$bankAccountId}
                  ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $res = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $res[] = $row;
        }

        return $res;
    }
}