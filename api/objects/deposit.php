<?php

class Deposit
{
    private $conn;
    private $table_name = "deposits";

    public $id;
    public $bank_account_id;
    public $amount;
    public $date_created;
    
    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getSumAndCountOfDepositsByDate($date) {
        $date = htmlspecialchars(strip_tags($date));

        $query = "SELECT SUM(d.amount) as deposit_sum, COUNT(d.id) as deposit_count
        FROM {$this->table_name} as d
        WHERE date_created LIKE '{$date}%'";

        // prepare query
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;        
    }

    function create()
    {       
        // query to insert record
        $query = "INSERT INTO {$this->table_name} SET bank_account_id=:bank_account_id, amount=:amount, date_created=:date_created ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->bank_account_id = htmlspecialchars(strip_tags($this->bank_account_id));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->date_created = htmlspecialchars(strip_tags($this->date_created));

        // bind values
        $stmt->bindParam(":bank_account_id", $this->bank_account_id);
        $stmt->bindParam(":amount", $this->amount);
        $stmt->bindParam(":date_created", $this->date_created);
        // execute query

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    function increaseAmount($increasePercent, $increaseDate) {
        $increasePercent = floatval($increasePercent);
        $increaseDate = htmlspecialchars(strip_tags($increaseDate));

        $query = "START TRANSACTION;
            UPDATE {$this->table_name} SET amount = amount + ({$increasePercent} * amount / 100) WHERE date_created LIKE '{$increaseDate}%';
            COMMIT;";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

}