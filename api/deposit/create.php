<?php
include_once ROOT . 'api/objects/Deposit.php';

$obj = new Deposit($db);
$data = (object)$_POST;

$obj->bank_account_id = $data->bank_account_id;
$obj->amount = $data->amount;
$obj->date_created = $data->date_created;

if ($obj->create()) {
    http_response_code(200);
    echo json_encode(array("message" => "Deposit added"));
}
else {
    http_response_code(503);
    echo json_encode(array("message" => "Error occurred while adding deposit"));
}

?>