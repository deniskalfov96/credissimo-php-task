class DepositSystem {

    constructor(depositSystemContainerSelector, apiRoute) {
        this.depositSystemContainer = document.querySelector(depositSystemContainerSelector);
        this.reportDate = new Date();
        this.depositSystemContainer.querySelector('.today-date'). innerHTML = this.reportDate;
        this.apiRoute = apiRoute;
        this.users = this.getUsers();
    }

    getUsers() {
        const getUsersUrl = this.apiRoute + 'users/read';
        return this.makeAjaxCall(getUsersUrl, 'GET');
    }
    
    makeAjaxCall(url, methodType, data = {}) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: methodType,
                data: data,
                success: function(data) {
                    resolve(data)
                },
                error: function(error) {
                    reject(error)
                },
            })
        })
    } 

    appendUsersWithDeposits() {
        this.users.then((users) => {
            users = JSON.parse(users);

            let usersDepositList = this.depositSystemContainer.querySelector('.list-users-deposits');
            usersDepositList.innerHTML = '';

            Object.values(users).map((user) => {

                let usersUl = document.createElement('ul');
                let usersUlLi = document.createElement('li');
                usersUlLi.innerHTML = '<b>' + user.name + '</b>';

                // Bank accounts
                if (user.bank_accounts.length > 0) {
                    let bankAccountUl = document.createElement('ul');
                    
                    Object.values(user.bank_accounts).map((ba) => {
                        let bankAccountUlLi = document.createElement('li');
                        bankAccountUlLi.innerHTML = '<u>' + ba.iban + '</u>';

                        let addDepositButton = document.createElement('button');
                        addDepositButton.addEventListener('click', () => this.appendAddDepositContent(ba.id, ba.iban));
                        addDepositButton.classList.add('mleft-20');
                        addDepositButton.innerHTML = "Add deposit";
                        bankAccountUlLi.appendChild(addDepositButton)

                        // Deposit
                        if (ba.deposits.length > 0) {
                            let depositUl = document.createElement('ul');
                        
                            Object.values(ba.deposits).map((dp) => {
                                let depositUlLi = document.createElement('li');
                                let depositDate = new Date(dp.date_created);
                                depositDate = depositDate.getDate() + '.' + parseInt(depositDate.getMonth()+1) + '.' + depositDate.getFullYear();
                                depositUlLi.innerHTML = dp.amount + ' ' + ba.currency + ' (' + depositDate + ')';
                                depositUl.appendChild(depositUlLi);
                            })
                        
                            bankAccountUlLi.appendChild(depositUl)
                        }

                        bankAccountUl.appendChild(bankAccountUlLi);
                    });

                    usersUlLi.appendChild(bankAccountUl)
                }

                usersUl.appendChild(usersUlLi);
                usersDepositList.appendChild(usersUl);
            });
        });
    }

    appendAddDepositContent(bankAccountId, bankAccountIban) {
        let addDepositContent = document.createElement('div');

        let addDepositInfoText = document.createElement('p');
        addDepositInfoText.innerHTML = 'Adding deposit into IBAN: ' + bankAccountIban;
        addDepositContent.appendChild(addDepositInfoText);

        let addDepositInput = document.createElement('input');
        addDepositInput.type = 'number';
        addDepositInput.min = '1';
        addDepositInput.value = '1';
        addDepositContent.appendChild(addDepositInput)

        let addDepositButton = document.createElement('button');
        addDepositButton.classList.add('mleft-20');
        addDepositButton.innerHTML = 'Add deposit';
        addDepositButton.addEventListener('click', () => this.addDeposit(bankAccountId, addDepositInput.value));
        addDepositContent.appendChild(addDepositButton)
    
        let addNewDepositContainer = this.depositSystemContainer.querySelector('.add-new-deposit');
        addNewDepositContainer.innerHTML = '';
        addNewDepositContainer.appendChild(addDepositContent)
    }

    addDeposit(bankAccountId, depositValue) {
        const addDepositUrl = this.apiRoute + 'deposit/create';
        this.makeAjaxCall(addDepositUrl, 'POST', { bank_account_id: bankAccountId, amount: depositValue, date_created: this.reportDate.getFullYear() + '-' + parseInt(this.reportDate.getMonth() + 1) + '-' + this.reportDate.getDate() + ' ' + this.reportDate.getHours() + ':' + this.reportDate.getMinutes() + ':' + this.reportDate.getSeconds()});

        this.users = this.getUsers();
        this.appendUsersWithDeposits();
    }

    
    generateReport() {
        
        const getReportUrl = this.apiRoute + 'report/daily?reportDate='+this.reportDate.getFullYear() + '-' + parseInt(this.reportDate.getMonth() + 1) + '-' + this.reportDate.getDate();
        const dailyReportData = this.makeAjaxCall(getReportUrl, 'GET');

        dailyReportData.then((reportData) => {
            reportData = JSON.parse(reportData);

            let reportTable = document.createElement('table');
            reportTable.classList.add('report-table');
            let reportTableTrHeader = document.createElement('tr');
            
            let reportTableThHeader = document.createElement('th');
            reportTableThHeader.innerHTML = 'Date';
            reportTableTrHeader.appendChild(reportTableThHeader)

            reportTableThHeader = document.createElement('th');
            reportTableThHeader.innerHTML = 'Increase';
            reportTableTrHeader.appendChild(reportTableThHeader)

            reportTableThHeader = document.createElement('th');
            reportTableThHeader.innerHTML = 'New deposits';
            reportTableTrHeader.appendChild(reportTableThHeader)
            
            reportTable.appendChild(reportTableTrHeader);
            
            Object.values(reportData).map((r) => {
                let reportTableTr = document.createElement('tr');

                let reportTableTd = document.createElement('td');
                reportTableTd.innerHTML = r.date_created;
                reportTableTr.appendChild(reportTableTd);

                reportTableTd = document.createElement('td');
                reportTableTd.innerHTML = 'BGN ' + r.increase_amount + (r.increase_amount_usd ? ' / USD ' + r.increase_amount_usd : '');
                reportTableTr.appendChild(reportTableTd);

                reportTableTd = document.createElement('td');
                reportTableTd.innerHTML = r.count_deposits;
                reportTableTr.appendChild(reportTableTd);

                reportTable.appendChild(reportTableTr);
            });

            this.users = this.getUsers();
            this.appendUsersWithDeposits();    

            const reportContainer = this.depositSystemContainer.querySelector('.report');
            reportContainer.innerHTML = '';
            reportContainer.appendChild(reportTable);

        });

        // Set the current day to next
        this.reportDate.setDate(this.reportDate.getDate() + 1);
        this.depositSystemContainer.querySelector('.today-date'). innerHTML = this.reportDate;

    }
}

const b = new DepositSystem('.deposit-system-container', 'http://localhost/php-tasks/credissimo/master/api/');
b.appendUsersWithDeposits();

document.getElementById('simulateChangeDay').addEventListener('click', () => b.generateReport());
